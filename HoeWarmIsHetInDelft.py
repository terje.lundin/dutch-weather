# Script to extract weather data from https://weerindelft.nl
# Author: Terje Lundin
# Date: 10 Oct 19
# Ver: 1.0

import urllib.request

#This is the column index for the temperature value in the raw input file
TEMPERATURE_INDEX = 4

temperature_celsius_rounded = None
try:
    # fetch the raw weather data
    url = urllib.request.urlopen('https://weerindelft.nl/clientraw.txt')
    byte_data = url.read()
    # convert byte data to a string to be able to split the data into columns
    weather_raw_data = "".join(map(chr, byte_data))
    if weather_raw_data:
        # raw weather data must have at least TEMPERATURE_INDEX entries, where the fourth is the temperature.
        # Number always assume to be correct format. If not, you could try/except on ValueError
        weather_data_array = weather_raw_data.split(" ")
        if len(weather_data_array) > TEMPERATURE_INDEX:
            temperature_celsius_rounded = round(float(weather_data_array[TEMPERATURE_INDEX]))
except urllib.error.HTTPError:
    pass

if temperature_celsius_rounded is None:
    print("Weather data currently not available. Try later")
else:
    print(temperature_celsius_rounded, "degrees Celsius")
